/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Roberto Jimenez
 */
public class Manager
{
    public static void main(String[] args) 
    {
      ModeloCuatroPuertas autoCuatro = new ModeloCuatroPuertas();
      Carro carro=new Carro(autoCuatro);
      carro.venta();
      
      ModeloCuatroPuertas miCarro = new ModeloCuatroPuertas();
      carro=new Carro(miCarro);
      carro.venta();
 
      ModeloDosPuertas dosPuertas = new ModeloDosPuertas();
      carro= new Carro(dosPuertas);
      carro.usar();
      
      Modelo4X4 cuatroX = new Modelo4X4();
      carro= new Carro(cuatroX);
      carro.usar();
   }
}