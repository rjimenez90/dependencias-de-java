/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Roberto Jimenez
 */
public class Carro 
{
    private ModeloIterfaz modelo;
    public Carro (ModeloIterfaz modeloGenerico)
    {
        modelo=modeloGenerico;
    }

    public void usar()
    {
        System.out.println("El auto que usted ha comprado es un:");
        modelo.comprar();
    }
    public void venta()
    {
        modelo.vender();
    }
}
